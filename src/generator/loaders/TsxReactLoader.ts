import { transform } from '../utils/babelUtils';
import { packModule } from '../utils/loaderUtils';
import { Loader } from '../../@types/loader';

const TsxReactLoader: Loader = {
  test: /.*\.tsx$/,
  load(fileName: string, content: string): string {
    const presets = ['react', 'typescript'];
    const plugins = ['proposal-class-properties', 'syntax-object-rest-spread'];
    const { code: moduleDefinition } = transform(content, fileName, {
      presets,
      plugins,
    });

    return packModule(fileName, moduleDefinition);
  },
};

export default TsxReactLoader;
