import { transform } from '../utils/babelUtils';
import { packModule } from '../utils/loaderUtils';
import { Loader } from '../../@types/loader';

const TsLoader: Loader = {
  test: /.*\.ts$/,
  load(fileName: string, content: string): string {
    const presets = ['typescript'];
    const { code: moduleDefinition } = transform(content, fileName, { presets });

    return packModule(fileName, moduleDefinition);
  },
};

export default TsLoader;
