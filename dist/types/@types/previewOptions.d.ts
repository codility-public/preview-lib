import { DependenciesMap } from './dependenciesMap';
import { Loader } from './loader';
export interface PreviewOptions {
    debug?: boolean;
    loaders: Loader[];
    dependenciesMap: DependenciesMap;
}
