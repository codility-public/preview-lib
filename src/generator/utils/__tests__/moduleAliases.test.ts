import {
  getModuleAliasesFromFiles,
  getUniqueFileNamesWithoutExts,
  findAliasForName,
  createAliasAMDModule,
} from '../moduleAliases';

describe('getModuleAliasesFromFiles', () => {
  it('generates module definitions which match snapshot', () => {
    const files = {
      'index.js': '',
      'solution.js': '',
      'solution.ts': '',
      'component.tsx': '',
    };
    const aliasModuleDefinitions = getModuleAliasesFromFiles(files);
    expect(aliasModuleDefinitions).toMatchSnapshot();
  });
});

describe('getUniqueFileNamesWithoutExts', () => {
  it('returns unique file names without file extensions', () => {
    const fileNames = {
      'index.js': '',
      'index.ts': '',
      'hi.js': '',
      'test.json': '',
    };

    expect(getUniqueFileNamesWithoutExts(fileNames)).toEqual(['index', 'hi', 'test']);
  });
});

describe('findAliasForName', () => {
  it('return first possible alias from files by specific order', () => {
    const fileNames = {
      'index.js': '',
      'index.ts': '',
      'hi.js': '',
      'test.json': '',
    };

    expect(findAliasForName('index', fileNames)).toEqual('index.js');
  });
});

describe('createAliasAMDModule', () => {
  it('match snapshot for index.js alias', () => {
    const result = createAliasAMDModule('index', 'index.js');
    expect(result).toMatchSnapshot();
  });
});
