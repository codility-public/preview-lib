"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var JsLoader_1 = require("../JsLoader");
var utils_1 = require("./utils");
describe('JsLoader', function () {
    var file = {
        fileName: 'index.js',
        content: "\n      import sort from 'sorting-library';\n\n      function sortFiles(files) {\n        return [...files].sort();\n      }\n\n      export const SOME_CONSTANT = 123;\n\n      export default sortFiles;\n    ",
    };
    it('match snapshot for js file', function () {
        var result = JsLoader_1.default.load(file.fileName, file.content);
        expect(result).toMatchSnapshot();
    });
    utils_1.generateTestsForMatcher(JsLoader_1.default, 'js');
});
//# sourceMappingURL=JsLoader.test.js.map