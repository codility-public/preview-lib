"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var babelPluginVueJsx = require("babel-plugin-transform-vue-jsx");
var injectStyles_1 = require("../utils/injectStyles");
var babelUtils_1 = require("../utils/babelUtils");
var loaderUtils_1 = require("../utils/loaderUtils");
// extract styles, scripts and template to different values
function parse(code) {
    if (code === void 0) { code = ''; }
    var element = document.createElement('div');
    element.innerHTML = code.trim();
    try {
        var templateTag = element.querySelector('template');
        var scriptTag = element.querySelector('script');
        var styleTags = Array.from(element.querySelectorAll('style')).map(function (node) { return node.innerHTML; });
        return {
            template: templateTag ? templateTag.innerHTML : '',
            script: scriptTag ? scriptTag.innerHTML : '',
            styles: styleTags,
        };
    }
    catch (error) {
        return { error: error };
    }
}
exports.parse = parse;
function createWrapper(moduleName, template) {
    return "\n    import VueComponent from '" + moduleName + "';\n    export * from '" + moduleName + "';\n\n    export default {...VueComponent, template: `" + template + "` };\n  ";
}
var VueLoader = {
    test: /.*\.vue$/,
    load: function (fileName, content) {
        var parseResult = parse(content);
        if ('error' in parseResult) {
            throw new Error("Loader for *.vue files can't parse file" + fileName);
        }
        var template = parseResult.template, script = parseResult.script, styles = parseResult.styles;
        var codeToTranspile = script + (styles ? injectStyles_1.default(styles.join('\n')) : '');
        babelUtils_1.registerPlugin('transform-vue-jsx', babelPluginVueJsx);
        var moduleDefinition = babelUtils_1.transform(codeToTranspile, fileName, {
            plugins: ['transform-vue-jsx'],
        }).code;
        // We need to inject template into Vue component
        // To make it easier we deal with it on runtime
        // First we create 'internal' dependency which consists of script / styles code
        var internalModuleName = loaderUtils_1.fileNameAsInternalPackage(fileName);
        var internalModule = loaderUtils_1.nameAMDModule(internalModuleName, moduleDefinition);
        // After that we create target module, which will be public
        // This module will import internal module and will inject template on runtime
        var wrapperModuleCode = createWrapper(internalModuleName, template);
        var wrapperModuleDefinition = babelUtils_1.transform(wrapperModuleCode, fileName).code;
        var namedWrapperModule = loaderUtils_1.packModule(fileName, wrapperModuleDefinition);
        // Finally we export 2 module definitions not one
        return internalModule + "\n" + namedWrapperModule;
    },
};
exports.default = VueLoader;
//# sourceMappingURL=VueLoader.js.map