"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function skipSingleQuote(text) {
    return text.replace(/'/g, '"');
}
function injectStyle(style) {
    return "\n        const linkElement = document.createElement('link');\n          linkElement.setAttribute('rel', 'stylesheet');\n          linkElement.setAttribute('type', 'text/css');\n          linkElement.setAttribute(\n            'href',\n            '" + ("data:text/css;charset=UTF-8," + encodeURIComponent(skipSingleQuote(style))) + "',\n            );\n        const head = document.head || document.getElementsByTagName('head')[0];\n        head.appendChild(linkElement);      \n    ";
}
exports.default = injectStyle;
//# sourceMappingURL=injectStyles.js.map