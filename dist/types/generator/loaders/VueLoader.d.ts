import { Loader } from '../../@types/loader';
interface ParseError {
    error: string;
}
interface ParseSuccess {
    script: string;
    template: string;
    styles: string[];
}
declare type IParseResult = ParseError | ParseSuccess;
export declare function parse(code?: string): IParseResult;
declare const VueLoader: Loader;
export default VueLoader;
