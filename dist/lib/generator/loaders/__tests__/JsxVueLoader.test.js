"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var JsxVueLoader_1 = require("../JsxVueLoader");
var utils_1 = require("./utils");
describe('JsxVueLoader', function () {
    var file = {
        fileName: 'index.js',
        content: "\n    import AnchoredHeading from './AnchoredHeading.vue'\n\n    new Vue({\n      el: '#demo',\n      render: function (h) {\n        return (\n          <AnchoredHeading level={1}>\n            <span>Hello</span> world!\n          </AnchoredHeading>\n        )\n      }\n    })\n    ",
    };
    it('match snapshot for js file', function () {
        var result = JsxVueLoader_1.default.load(file.fileName, file.content);
        expect(result).toMatchSnapshot();
    });
    utils_1.generateTestsForMatcher(JsxVueLoader_1.default, ['jsx', 'js']);
});
//# sourceMappingURL=JsxVueLoader.test.js.map