export interface DependenciesMap {
  [key: string]: string;
}
