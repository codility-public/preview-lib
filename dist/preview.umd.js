(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@babel/standalone'), require('babel-plugin-transform-vue-jsx'), require('@vue/babel-preset-jsx')) :
    typeof define === 'function' && define.amd ? define(['exports', '@babel/standalone', 'babel-plugin-transform-vue-jsx', '@vue/babel-preset-jsx'], factory) :
    (global = global || self, factory(global.previewServer = {}, global.standalone, global.babelPluginVueJsx, global.vueBabelPresetJsx));
}(this, (function (exports, standalone, babelPluginVueJsx, vueBabelPresetJsx) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    }

    function setupCommunication(reload, debugLog) {
        function sendMessage(type, data) {
            debugLog("[OUT] Message from preview: " + JSON.stringify({ type: type, data: data }));
            window.parent.postMessage(__assign({ type: type }, data), '*');
        }
        /**
         * Handle general errors that are not being logged using the console
         */
        window.addEventListener('error', function () {
            sendMessage('error');
        });
        function loadSolution(solution) {
            if (solution) {
                try {
                    reload(solution);
                }
                catch (err) {
                    console.error(err);
                }
                sendMessage('solutionLoaded');
            }
        }
        window.addEventListener('message', function (message) {
            debugLog("[IN] Message to preview: " + JSON.stringify(message));
            if (message.data.files) {
                loadSolution(message.data.files);
            }
            if (message.data.solution) {
                loadSolution({ 'index.js': message.data.solution });
            }
        });
        /**
         * The iframe content is loaded at this point
         */
        sendMessage('loaded');
        return sendMessage;
    }

    var PreviewClient = /** @class */ (function () {
        function PreviewClient(options) {
            var _this = this;
            this.reload = function (solutionSource) {
                _this.debugLog('Transpiling code.');
                try {
                    _this.updateIframe(_this.previewGenerator.generate(solutionSource));
                }
                catch (error) {
                    console.error(error);
                }
                _this.debugLog('Code has been transpiled.');
            };
            this.debugLog = function (message) {
                if (_this.debug) {
                    console.log(message);
                }
            };
            var previewGenerator = options.previewGenerator, debug = options.debug;
            this.sendMessage = null;
            this.debug = debug || false;
            this.previewGenerator = previewGenerator;
        }
        PreviewClient.prototype.updateIframe = function (html) {
            this.debugLog('Updating preview iframe.');
            var iframe = document.getElementById('preview');
            if (iframe) {
                iframe.setAttribute('srcdoc', html);
            }
            else {
                var newIframe = document.createElement('iframe');
                newIframe.setAttribute('id', 'preview');
                newIframe.setAttribute('srcdoc', html);
                var body = document.body || document.getElementsByTagName('body')[0];
                body.setAttribute('style', 'margin: 0;');
                body.appendChild(newIframe);
            }
            this.debugLog('Iframe has been updated.');
        };
        PreviewClient.prototype.init = function () {
            var styleRules = document.createElement('style');
            styleRules.innerHTML = "#preview { width: 100vw; height: 100vh; border: 0; }\n      body { margin: 0; }\n    ";
            var head = document.head || document.getElementsByTagName('head')[0];
            head.appendChild(styleRules);
            this.sendMessage = setupCommunication(this.reload, this.debugLog);
        };
        return PreviewClient;
    }());

    var PACKAGE_NAME = 'codility-solution';
    var INTERNAL_PACKAGE = '@@@internals';
    function fileNameWithPackageRoot(fileName) {
        return PACKAGE_NAME + "/" + fileName;
    }
    function fileNameAsInternalPackage(fileName) {
        return INTERNAL_PACKAGE + "/" + fileName;
    }
    function nameAMDModule(packageName, module) {
        var startString = 'define([';
        if (module.indexOf(startString) === 1) {
            throw new Error('Transpiled module is not AMD. Something is wrong with babel plugins.');
        }
        return module.replace('define([', "define(\"" + packageName + "\",[");
    }
    function packModule(packageName, module) {
        return nameAMDModule(fileNameWithPackageRoot(packageName), module);
    }

    function createPreview(moduleDefinitions, dependenciesMap) {
        var stringifyDependenciesMap = JSON.stringify(dependenciesMap);
        var code = moduleDefinitions.join('\n');
        return "<html>\n                <head>\n                    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.6/require.js\"></script>\n                    <script>requirejs.config({\n                            paths: " + stringifyDependenciesMap + ",\n                            packages: [\"" + PACKAGE_NAME + "\"]\n                        });\n                    </script>\n                    <script>" + code + "</script>\n                    <script>\n                    function docReady(fn) {\n                        if (document.readyState === \"complete\" || document.readyState === \"interactive\") {\n                            setTimeout(fn, 0);\n                        } else {\n                            document.addEventListener(\"DOMContentLoaded\", fn);\n                        }\n                    }\n                    </script>\n                </head>\n                <body>\n                    <div id=\"root\"></div>\n                    <script>\n                        docReady(async function() {\n                            window.requirejs(['codility-solution/index.js'], function mountingPoint() {});\n                        });\n                    </script>\n                </body>\n            </html>\n            ";
    }

    function registerPlugin(name, plugin) {
        if (!standalone.availablePlugins[name]) {
            standalone.registerPlugin(name, plugin);
        }
    }
    function registerPreset(name, preset) {
        if (!standalone.availablePresets[name]) {
            standalone.registerPreset(name, preset);
        }
    }
    function transform(code, fileName, options) {
        if (options === void 0) { options = {}; }
        var _a = __assign({ plugins: [], presets: [] }, options), plugins = _a.plugins, presets = _a.presets;
        var transpiledAMDModule = standalone.transform(code, {
            presets: __spreadArrays([
                [
                    'env',
                    {
                        targets: {
                            chrome: '45',
                            edge: '16',
                            firefox: '45',
                            ie: '11',
                            opera: '40',
                            safari: '9',
                        },
                    },
                ]
            ], presets),
            plugins: __spreadArrays(plugins, ['transform-modules-amd']),
            filename: fileName,
        }).code;
        return { code: transpiledAMDModule };
    }

    var DEFAULT_EXTENSIONS_ORDER = ['js', 'jsx', 'ts', 'tsx'];
    function getUniqueFileNamesWithoutExts(files) {
        var fileExtRegexp = /(.*)\.([A-z]*)$/;
        var resultMap = {};
        Object.keys(files).forEach(function (fileName) {
            var match = fileName.match(fileExtRegexp);
            resultMap[match !== null && match.length >= 2 ? match[1] : fileName] = '';
        });
        return Object.keys(resultMap);
    }
    function findAliasForName(name, files) {
        var possibleDefaultImports = DEFAULT_EXTENSIONS_ORDER.map(function (currExt) { return name + "." + currExt; });
        return possibleDefaultImports.find(function (possibleImport) { return possibleImport in files; });
    }
    function createAliasAMDModule(alias, moduleWithAlias) {
        var aliasedModuleName = fileNameWithPackageRoot(moduleWithAlias);
        var code = "\n    import defExport from '" + aliasedModuleName + "';\n    export * from '" + aliasedModuleName + "';\n    export default defExport;\n  ";
        var aliasModuleDefinition = transform(code, alias).code;
        return nameAMDModule(fileNameWithPackageRoot(alias), aliasModuleDefinition);
    }
    function getModuleAliasesFromFiles(files) {
        var fileNames = getUniqueFileNamesWithoutExts(files);
        var moduleAliasesMap = {};
        fileNames.forEach(function (fileNameWithoutExt) {
            if (!(fileNameWithoutExt in moduleAliasesMap)) {
                var alias = findAliasForName(fileNameWithoutExt, files);
                if (alias) {
                    moduleAliasesMap[fileNameWithoutExt] = alias;
                }
            }
        });
        return Object.entries(moduleAliasesMap).map(function (_a) {
            var alias = _a[0], moduleName = _a[1];
            return createAliasAMDModule(alias, moduleName);
        });
    }

    var PreviewGenerator = /** @class */ (function () {
        function PreviewGenerator(options) {
            var loaders = options.loaders, dependenciesMap = options.dependenciesMap;
            this.loaders = loaders;
            this.defaultDependenciesMap = dependenciesMap;
        }
        PreviewGenerator.prototype.findLoaderForFile = function (fileName) {
            if (this.loaders.length === 0) {
                return null;
            }
            var isLoaderMatched = function (loader) { return Boolean(fileName.match(loader.test)); };
            var loadersForFileType = this.loaders.filter(isLoaderMatched);
            return loadersForFileType[0];
        };
        PreviewGenerator.prototype.getModuleDefinitions = function (files) {
            var _this = this;
            var moduleDefinitions = [];
            Object.entries(files).forEach(function (_a) {
                var fileName = _a[0], solutionSource = _a[1];
                var loader = _this.findLoaderForFile(fileName);
                if (!loader) {
                    throw new Error("There's no loader defined for " + fileName + ", please check if the file extension is correct.");
                }
                moduleDefinitions = __spreadArrays(moduleDefinitions, [loader.load(fileName, solutionSource)]);
            });
            return moduleDefinitions;
        };
        PreviewGenerator.prototype.getModuleAliases = function (files) {
            return getModuleAliasesFromFiles(files);
        };
        PreviewGenerator.prototype.generate = function (files, dependenciesMap) {
            var moduleDefinitions = this.getModuleDefinitions(files);
            var moduleAliases = this.getModuleAliases(files);
            var finalDependenciesMap = dependenciesMap ? dependenciesMap : this.defaultDependenciesMap;
            return createPreview(moduleDefinitions.concat(moduleAliases), finalDependenciesMap);
        };
        return PreviewGenerator;
    }());

    function skipSingleQuote(text) {
        return text.replace(/'/g, '"');
    }
    function injectStyle(style) {
        return "\n        const linkElement = document.createElement('link');\n          linkElement.setAttribute('rel', 'stylesheet');\n          linkElement.setAttribute('type', 'text/css');\n          linkElement.setAttribute(\n            'href',\n            '" + ("data:text/css;charset=UTF-8," + encodeURIComponent(skipSingleQuote(style))) + "',\n            );\n        const head = document.head || document.getElementsByTagName('head')[0];\n        head.appendChild(linkElement);      \n    ";
    }

    // extract styles, scripts and template to different values
    function parse(code) {
        if (code === void 0) { code = ''; }
        var element = document.createElement('div');
        element.innerHTML = code.trim();
        try {
            var templateTag = element.querySelector('template');
            var scriptTag = element.querySelector('script');
            var styleTags = Array.from(element.querySelectorAll('style')).map(function (node) { return node.innerHTML; });
            return {
                template: templateTag ? templateTag.innerHTML : '',
                script: scriptTag ? scriptTag.innerHTML : '',
                styles: styleTags,
            };
        }
        catch (error) {
            return { error: error };
        }
    }
    function createWrapper(moduleName, template) {
        return "\n    import VueComponent from '" + moduleName + "';\n    export * from '" + moduleName + "';\n\n    export default {...VueComponent, template: `" + template + "` };\n  ";
    }
    var VueLoader = {
        test: /.*\.vue$/,
        load: function (fileName, content) {
            var parseResult = parse(content);
            if ('error' in parseResult) {
                throw new Error("Loader for *.vue files can't parse file" + fileName);
            }
            var template = parseResult.template, script = parseResult.script, styles = parseResult.styles;
            var codeToTranspile = script + (styles ? injectStyle(styles.join('\n')) : '');
            registerPlugin('transform-vue-jsx', babelPluginVueJsx);
            var moduleDefinition = transform(codeToTranspile, fileName, {
                plugins: ['transform-vue-jsx'],
            }).code;
            // We need to inject template into Vue component
            // To make it easier we deal with it on runtime
            // First we create 'internal' dependency which consists of script / styles code
            var internalModuleName = fileNameAsInternalPackage(fileName);
            var internalModule = nameAMDModule(internalModuleName, moduleDefinition);
            // After that we create target module, which will be public
            // This module will import internal module and will inject template on runtime
            var wrapperModuleCode = createWrapper(internalModuleName, template);
            var wrapperModuleDefinition = transform(wrapperModuleCode, fileName).code;
            var namedWrapperModule = packModule(fileName, wrapperModuleDefinition);
            // Finally we export 2 module definitions not one
            return internalModule + "\n" + namedWrapperModule;
        },
    };

    var JsxVueLoader = {
        // it works with not only jsx but also js
        test: /.*\.jsx?$/,
        load: function (fileName, content) {
            registerPreset('vue-babel-preset-jsx', vueBabelPresetJsx);
            var presets = ['vue-babel-preset-jsx'];
            var plugins = ['proposal-class-properties', 'syntax-object-rest-spread'];
            var moduleDefinition = transform(content, fileName, {
                presets: presets,
                plugins: plugins,
            }).code;
            return packModule(fileName, moduleDefinition);
        },
    };

    var CssLoader = {
        test: /.*\.css$/,
        load: function (fileName, content) {
            // create AMD module that will inject styles to document on load
            return "define(\"" + fileNameWithPackageRoot(fileName) + "\", [], function () {\n            " + injectStyle(content) + "\n        });";
        },
    };

    var JsLoader = {
        test: /.*\.js$/,
        load: function (fileName, content) {
            var moduleDefinition = transform(content, fileName).code;
            return packModule(fileName, moduleDefinition);
        },
    };

    var JsonLoader = {
        test: /.*\.json$/,
        load: function (fileName, content) {
            try {
                // Parse to validate JSON
                JSON.parse(content);
                var wrappedJSON = "export default " + content + ";";
                var moduleDefinition = transform(wrappedJSON, fileName).code;
                return packModule(fileName, moduleDefinition);
            }
            catch (error) {
                throw new Error(fileName + " has error in the structure. Details: " + error.message);
            }
        },
    };

    var TsLoader = {
        test: /.*\.ts$/,
        load: function (fileName, content) {
            var presets = ['typescript'];
            var moduleDefinition = transform(content, fileName, { presets: presets }).code;
            return packModule(fileName, moduleDefinition);
        },
    };

    // it works with not only jsx but also js
    var JsxReactLoader = {
        test: /.*\.jsx?$/,
        load: function (fileName, content) {
            var presets = ['react'];
            var plugins = ['proposal-class-properties', 'syntax-object-rest-spread'];
            var moduleDefinition = transform(content, fileName, {
                presets: presets,
                plugins: plugins,
            }).code;
            return packModule(fileName, moduleDefinition);
        },
    };

    var TsxReactLoader = {
        test: /.*\.tsx$/,
        load: function (fileName, content) {
            var presets = ['react', 'typescript'];
            var plugins = ['proposal-class-properties', 'syntax-object-rest-spread'];
            var moduleDefinition = transform(content, fileName, {
                presets: presets,
                plugins: plugins,
            }).code;
            return packModule(fileName, moduleDefinition);
        },
    };

    var loaders = {
        vueLoader: VueLoader,
        jsxVueLoader: JsxVueLoader,
        cssLoader: CssLoader,
        jsonLoader: JsonLoader,
        jsLoader: JsLoader,
        tsLoader: TsLoader,
        jsxLoader: JsxReactLoader,
        tsxLoader: TsxReactLoader,
    };

    var jsxLoader = loaders.jsxLoader, cssLoader = loaders.cssLoader, jsonLoader = loaders.jsonLoader;
    var loaders$1 = [jsxLoader, cssLoader, jsonLoader];
    // This is for development only, if you want to publish preview please upload required files with it
    var dependenciesMap = {
        react: 'https://unpkg.com/react@16.10.1/umd/react.development',
        'react-dom': 'https://unpkg.com/react-dom@16.10.1/umd/react-dom.development',
    };

    var vueLoader = loaders.vueLoader, jsxVueLoader = loaders.jsxVueLoader, cssLoader$1 = loaders.cssLoader, jsonLoader$1 = loaders.jsonLoader;
    var loaders$2 = [vueLoader, jsxVueLoader, cssLoader$1, jsonLoader$1];
    // This is for development only, if you want to publish preview please upload required files with it
    var dependenciesMap$1 = {
        vue: 'https://unpkg.com/vue@2.6.10/dist/vue',
    };

    var reactConfig = {
        loaders: loaders$1,
        dependenciesMap: dependenciesMap,
    };
    var vueConfig = {
        loaders: loaders$2,
        dependenciesMap: dependenciesMap$1,
    };

    var loaderUtils = {
        registerPlugin: registerPlugin,
        registerPreset: registerPreset,
        transform: transform,
        loaders: loaders,
        packModule: packModule,
    };
    function configurePreview(options) {
        var debug = options.debug, loaders = options.loaders, dependenciesMap = options.dependenciesMap;
        var previewGenerator = new PreviewGenerator({ loaders: loaders, dependenciesMap: dependenciesMap });
        var client = new PreviewClient({ debug: debug, previewGenerator: previewGenerator });
        client.init();
    }

    exports.PreviewGenerator = PreviewGenerator;
    exports.configurePreview = configurePreview;
    exports.loaderUtils = loaderUtils;
    exports.reactConfig = reactConfig;
    exports.vueConfig = vueConfig;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=preview.umd.js.map
