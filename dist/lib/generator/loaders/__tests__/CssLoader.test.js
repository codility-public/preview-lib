"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CssLoader_1 = require("../CssLoader");
var utils_1 = require("./utils");
describe('CssLoader', function () {
    var cssFile = {
        fileName: 'index.css',
        content: "\n      .counter-button {\n        padding: .3rem .8rem;\n        border: 1px solid grey;\n      }\n\n      h2 {\n        font-size: 100px;\n      }\n    ",
    };
    it('match snapshot for simple css file', function () {
        var result = CssLoader_1.default.load(cssFile.fileName, cssFile.content);
        expect(result).toMatchSnapshot();
    });
    it('does not containt single quote for css file with it', function () {
        var cssSingleQuoteFile = {
            fileName: 'test.css',
            content: "\n        body {\n          font-family: 'Helvetica Nue';\n        }\n      ",
        };
        var result = CssLoader_1.default.load(cssSingleQuoteFile.fileName, cssSingleQuoteFile.content);
        expect(result).toMatchSnapshot();
    });
    utils_1.generateTestsForMatcher(CssLoader_1.default, 'css');
});
//# sourceMappingURL=CssLoader.test.js.map