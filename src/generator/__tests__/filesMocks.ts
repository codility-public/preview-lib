export const jsSimpleProject = {
  dependenciesMap: {
    react: 'https://unpkg.com/react@15.3.2/dist/react',
    'react-dom': 'https://unpkg.com/react-dom@15.3.2/dist/react-dom',
  },
  files: {
    'index.js': `
      import './styles/index.css';
      import getNiceText from './utils.js';


    `,
    'utils.js': `
      function getNiceText(text) {
        return text.trim().toLowerCase();
      }

      export default getNiceText;
    `,
    'styles/index.css': `
      h2 {
        font-size: 40px;
      }
      body {
        background-color: black;
        color: white;
      }
    `,
    'styles/stuff.json': `
      {
        "texts": ["HI", "TeEXT", "Babbbel"]
      }
    `,
  },
};

export const vueFiles = {
  'index.js': `
    import Vue from 'vue';
    import App from 'App.vue';

    const config = {
      ...App,
      el: '#root',
    };
    
    new Vue(config);
  `,
  'App.vue': `
    <script>
    import getNiceText from './utils.js';

    export default {
      name: 'App',
      data: {
        text: 'Some nice text',
      },
      computed: {
        niceText() {
          return getNiceText(this.text);
        }
      }
    }
    </script>

    <style>
    .text {
      font-size: 2rem;
      color: tomato;
    }
    </style>

    <template>
      <div class="text">
      {{text}}
      </div>
    </template>
  `,
  'utils.js': `
    function getNiceText(text) {
      return text.trim().toUpperCase();
    }

    export default getNiceText;
  `,
  'styles/index.css': `
    h2 {
      font-size: 40px;
    }
  `,
  'styles/stuff.json': `
    {
      "texts": ["HI", "TeEXT", "Babbbel"]
    }
  `,
};

export const reactFiles = {
  'index.jsx': `
    import React from 'react';
    import ReactDOM from 'react-dom';
    import App from 'App.jsx';

    ReactDOM.render(<App/>, document.getElementById('root'));
  `,
  'App.jsx': `
    import React from 'react';
    import stuff from './stuff.json';
    import { getNiceText } from './utils.js';

    class App extends React.Component { 
      render() {
        return (<ul class="nice-list">
                {stuf.texts.map(text => <li>{text}</li>)}
              </ul>);
      }
    }

    export default App;
  `,
  'utils.js': `
    export function getNiceText(text) {
      return text.trim().toUpperCase();
    }
  `,
  'styles/index.css': `
    nice-list {
      font-size: 20px;
      color: tomato;
    }
  `,
  'stuff.json': `
    {
      "texts": ["HI", "TeEXT", "Babbbel"]
    }
  `,
};
