import { DependenciesMap } from './dependenciesMap';
import { Loader } from './loader';
export interface GeneratorOptions {
    loaders: Loader[];
    dependenciesMap: DependenciesMap;
}
