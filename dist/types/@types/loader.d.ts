export interface Loader {
    test: RegExp;
    load: (fileName: string, content: string) => string;
}
