"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var babelUtils_1 = require("../utils/babelUtils");
var loaderUtils_1 = require("../utils/loaderUtils");
var JsonLoader = {
    test: /.*\.json$/,
    load: function (fileName, content) {
        try {
            // Parse to validate JSON
            JSON.parse(content);
            var wrappedJSON = "export default " + content + ";";
            var moduleDefinition = babelUtils_1.transform(wrappedJSON, fileName).code;
            return loaderUtils_1.packModule(fileName, moduleDefinition);
        }
        catch (error) {
            throw new Error(fileName + " has error in the structure. Details: " + error.message);
        }
    },
};
exports.default = JsonLoader;
//# sourceMappingURL=JsonLoader.js.map