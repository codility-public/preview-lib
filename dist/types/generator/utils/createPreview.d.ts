import { DependenciesMap } from '../../@types/dependenciesMap';
export default function createPreview(moduleDefinitions: string[], dependenciesMap: DependenciesMap): string;
