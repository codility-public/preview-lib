"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var babelUtils_1 = require("../utils/babelUtils");
var loaderUtils_1 = require("../utils/loaderUtils");
// it works with not only jsx but also js
var JsxReactLoader = {
    test: /.*\.jsx?$/,
    load: function (fileName, content) {
        var presets = ['react'];
        var plugins = ['proposal-class-properties', 'syntax-object-rest-spread'];
        var moduleDefinition = babelUtils_1.transform(content, fileName, {
            presets: presets,
            plugins: plugins,
        }).code;
        return loaderUtils_1.packModule(fileName, moduleDefinition);
    },
};
exports.default = JsxReactLoader;
//# sourceMappingURL=JsxReactLoader.js.map