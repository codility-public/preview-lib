import tsLoader from '../TsLoader';
import { generateTestsForMatcher } from './utils';

describe('TsLoader', () => {
  const file = {
    fileName: 'index.ts',
    content: `
      import sort from 'sorting-library';

      enum FileType {
        json = 'json',
        css = 'css'
      }

      interface File { 
        fileName: string,
        content: string,
        type: FileType
      }

      function sortFiles(files: File[]): File[] {
        return [...files].sort();
      }

      export const SOME_CONSTANT = 123;

      export default sortFiles;
    `,
  };

  it('match snapshot for ts file', () => {
    const result = tsLoader.load(file.fileName, file.content);
    expect(result).toMatchSnapshot();
  });

  generateTestsForMatcher(tsLoader, 'ts');
});
