import vueLoader from './VueLoader';
import jsxVueLoader from './JsxVueLoader';
import cssLoader from './CssLoader';
import jsLoader from './JsLoader';
import jsonLoader from './JsonLoader';
import tsLoader from './TsLoader';
import jsxLoader from './JsxReactLoader';
import tsxLoader from './TsxReactLoader';

export default {
  vueLoader,
  jsxVueLoader,
  cssLoader,
  jsonLoader,
  jsLoader,
  tsLoader,
  jsxLoader,
  tsxLoader,
};
