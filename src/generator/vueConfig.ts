import fileLoaders from './loaders';

const { vueLoader, jsxVueLoader, cssLoader, jsonLoader } = fileLoaders;

export const loaders = [vueLoader, jsxVueLoader, cssLoader, jsonLoader];

// This is for development only, if you want to publish preview please upload required files with it
export const dependenciesMap = {
  vue: 'https://unpkg.com/vue@2.6.10/dist/vue',
};
