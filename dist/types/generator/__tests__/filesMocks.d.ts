export declare const jsSimpleProject: {
    dependenciesMap: {
        react: string;
        'react-dom': string;
    };
    files: {
        'index.js': string;
        'utils.js': string;
        'styles/index.css': string;
        'styles/stuff.json': string;
    };
};
export declare const vueFiles: {
    'index.js': string;
    'App.vue': string;
    'utils.js': string;
    'styles/index.css': string;
    'styles/stuff.json': string;
};
export declare const reactFiles: {
    'index.jsx': string;
    'App.jsx': string;
    'utils.js': string;
    'styles/index.css': string;
    'stuff.json': string;
};
