import PreviewGenerator from '../generator/PreviewGenerator';
import { Files } from '../@types/files';
import { ClientOptions } from '../@types/clientOptions';
export default class PreviewClient {
    constructor(options: ClientOptions);
    previewGenerator: PreviewGenerator;
    sendMessage: ((type: string, data?: any) => void) | null;
    debug: boolean;
    updateIframe(html: string): void;
    reload: (solutionSource: Files) => void;
    init(): void;
    debugLog: (message: string) => void;
}
