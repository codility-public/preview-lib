import { registerPlugin, registerPreset, transform, packModule } from './generator';
import { PreviewOptions } from './@types/previewOptions';
export { PreviewGenerator, vueConfig, reactConfig } from './generator';
export declare const loaderUtils: {
    registerPlugin: typeof registerPlugin;
    registerPreset: typeof registerPreset;
    transform: typeof transform;
    loaders: {
        vueLoader: import("./@types/loader").Loader;
        jsxVueLoader: import("./@types/loader").Loader;
        cssLoader: import("./@types/loader").Loader;
        jsonLoader: import("./@types/loader").Loader;
        jsLoader: import("./@types/loader").Loader;
        tsLoader: import("./@types/loader").Loader;
        jsxLoader: import("./@types/loader").Loader;
        tsxLoader: import("./@types/loader").Loader;
    };
    packModule: typeof packModule;
};
export declare function configurePreview(options: PreviewOptions): void;
