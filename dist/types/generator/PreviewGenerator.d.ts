import { Loader } from '../@types/loader';
import { DependenciesMap } from '../@types/dependenciesMap';
import { Files } from '../@types/files';
import { GeneratorOptions } from '../@types/generatorOptions';
declare class PreviewGenerator {
    constructor(options: GeneratorOptions);
    loaders: Loader[];
    defaultDependenciesMap: DependenciesMap;
    findLoaderForFile(fileName: string): Loader | null;
    getModuleDefinitions(files: Files): Array<string>;
    getModuleAliases(files: Files): Array<string>;
    generate(files: Files, dependenciesMap?: DependenciesMap): string;
}
export default PreviewGenerator;
