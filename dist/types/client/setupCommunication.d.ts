import { Files } from '../@types/files';
export default function setupCommunication(reload: (solution: Files) => void, debugLog: (message: string) => void): (type: string, data?: any) => void;
