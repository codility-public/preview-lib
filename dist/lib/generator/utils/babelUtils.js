"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var standalone_1 = require("@babel/standalone");
function registerPlugin(name, plugin) {
    if (!standalone_1.availablePlugins[name]) {
        standalone_1.registerPlugin(name, plugin);
    }
}
exports.registerPlugin = registerPlugin;
function registerPreset(name, preset) {
    if (!standalone_1.availablePresets[name]) {
        standalone_1.registerPreset(name, preset);
    }
}
exports.registerPreset = registerPreset;
function transform(code, fileName, options) {
    if (options === void 0) { options = {}; }
    var _a = __assign({ plugins: [], presets: [] }, options), plugins = _a.plugins, presets = _a.presets;
    var transpiledAMDModule = standalone_1.transform(code, {
        presets: __spreadArrays([
            [
                'env',
                {
                    targets: {
                        chrome: '45',
                        edge: '16',
                        firefox: '45',
                        ie: '11',
                        opera: '40',
                        safari: '9',
                    },
                },
            ]
        ], presets),
        plugins: __spreadArrays(plugins, ['transform-modules-amd']),
        filename: fileName,
    }).code;
    return { code: transpiledAMDModule };
}
exports.transform = transform;
//# sourceMappingURL=babelUtils.js.map