import jsxVueLoader from '../JsxVueLoader';
import { generateTestsForMatcher } from './utils';

describe('JsxVueLoader', () => {
  const file = {
    fileName: 'index.js',
    content: `
    import AnchoredHeading from './AnchoredHeading.vue'

    new Vue({
      el: '#demo',
      render: function (h) {
        return (
          <AnchoredHeading level={1}>
            <span>Hello</span> world!
          </AnchoredHeading>
        )
      }
    })
    `,
  };

  it('match snapshot for js file', () => {
    const result = jsxVueLoader.load(file.fileName, file.content);
    expect(result).toMatchSnapshot();
  });

  generateTestsForMatcher(jsxVueLoader, ['jsx', 'js']);
});
