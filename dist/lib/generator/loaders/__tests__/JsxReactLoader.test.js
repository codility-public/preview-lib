"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var JsxReactLoader_1 = require("../JsxReactLoader");
var utils_1 = require("./utils");
describe('JsxReactLoader', function () {
    var file = {
        fileName: 'index.js',
        content: "\n    import React from 'react';\n    import sort from 'sorting-library';\n    import Component from './component.js';\n\n    function sortFiles(files) {\n      return [...files].sort();\n    }\n\n    export class Files extends React.Component {\n      // test class properties\n      doSomeStuff = () => { this.setState({stuff: true}) }; \n      \n      render() {\n        // test spread;\n        const x = ['a', 'b', 'c'];\n        const y = [...x, 'd'];\n        const { files } = this.props;\n        return (<div>\n                yesh\n                </div>);\n      }\n    }\n\n    export const SOME_CONSTANT = 123;\n\n    export default sortFiles;\n    ",
    };
    it('match snapshot for js file', function () {
        var result = JsxReactLoader_1.default.load(file.fileName, file.content);
        expect(result).toMatchSnapshot();
    });
    utils_1.generateTestsForMatcher(JsxReactLoader_1.default, ['jsx', 'js']);
});
//# sourceMappingURL=JsxReactLoader.test.js.map