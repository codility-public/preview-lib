import createPreview from './utils/createPreview';
import { Loader } from '../@types/loader';
import { getModuleAliasesFromFiles } from './utils/moduleAliases';
import { DependenciesMap } from '../@types/dependenciesMap';
import { Files } from '../@types/files';
import { GeneratorOptions } from '../@types/generatorOptions';

class PreviewGenerator {
  constructor(options: GeneratorOptions) {
    const { loaders, dependenciesMap } = options;
    this.loaders = loaders;
    this.defaultDependenciesMap = dependenciesMap;
  }

  loaders: Loader[];

  defaultDependenciesMap: DependenciesMap;

  findLoaderForFile(fileName: string): Loader | null {
    if (this.loaders.length === 0) {
      return null;
    }

    const isLoaderMatched = (loader: Loader): boolean => Boolean(fileName.match(loader.test));
    const loadersForFileType = this.loaders.find(isLoaderMatched);

    return loadersForFileType;
  }

  getModuleDefinitions(files: Files): Array<string> {
    let moduleDefinitions: string[] = [];

    Object.entries(files).forEach(([fileName, solutionSource]) => {
      const loader = this.findLoaderForFile(fileName);

      if (!loader) {
        throw new Error(
          `There's no loader defined for ${fileName}, please check if the file extension is correct.`,
        );
      }

      moduleDefinitions = [...moduleDefinitions, loader.load(fileName, solutionSource)];
    });

    return moduleDefinitions;
  }

  getModuleAliases(files: Files): Array<string> {
    return getModuleAliasesFromFiles(files);
  }

  generate(files: Files, dependenciesMap?: DependenciesMap): string {
    const moduleDefinitions = this.getModuleDefinitions(files);
    const moduleAliases = this.getModuleAliases(files);

    const finalDependenciesMap = dependenciesMap ? dependenciesMap : this.defaultDependenciesMap;

    return createPreview(moduleDefinitions.concat(moduleAliases), finalDependenciesMap);
  }
}

export default PreviewGenerator;
