"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var client_1 = require("./client");
var generator_1 = require("./generator");
var generator_2 = require("./generator");
exports.PreviewGenerator = generator_2.PreviewGenerator;
exports.vueConfig = generator_2.vueConfig;
exports.reactConfig = generator_2.reactConfig;
exports.loaderUtils = {
    registerPlugin: generator_1.registerPlugin,
    registerPreset: generator_1.registerPreset,
    transform: generator_1.transform,
    loaders: generator_1.loaders,
    packModule: generator_1.packModule,
};
function configurePreview(options) {
    var debug = options.debug, loaders = options.loaders, dependenciesMap = options.dependenciesMap;
    var previewGenerator = new generator_1.PreviewGenerator({ loaders: loaders, dependenciesMap: dependenciesMap });
    var client = new client_1.PreviewClient({ debug: debug, previewGenerator: previewGenerator });
    client.init();
}
exports.configurePreview = configurePreview;
//# sourceMappingURL=index.js.map