import vueLoader, { parse } from '../VueLoader';
import { generateTestsForMatcher } from './utils';

describe('VueLoader', () => {
  const template = '<div class="component"></div>';
  const style = `
  .component { 
    width: 200px;
    hieght: 400px;
    background-color: red;
  }
  `;
  const script = `
  export default {
    name: 'component'
  }
  `;
  const vueFile = {
    fileName: 'component.vue',
    content: `
      <template>
      ${template}
      </template>

      <script>
      ${script}
      </script>

      <style>
      ${style}
      </style>
    `,
  };

  it('parse function extracts template, style, script to different props', () => {
    const parseResult = parse(vueFile.content);
    const noErrorFound = !('error' in parseResult);

    expect(noErrorFound).toEqual(true);

    // condition duplicated beacause of dummy TS evaluator
    if (!('error' in parseResult)) {
      const { template: parsedTemplate, script: parsedScript, styles: parsedStyles } = parseResult;
      expect(parsedTemplate.trim()).toEqual(template.trim());
      expect(parsedScript.trim()).toEqual(parsedScript.trim());
      expect(parsedStyles.length).toEqual(1);
      expect(parsedStyles[0].trim()).toEqual(style.trim());
    }
  });

  it('parse function works when tags are undefined', () => {
    expect(() => parse('')).not.toThrowError();
    const parseResult = parse('');
    const noErrorFound = !('error' in parseResult);

    expect(noErrorFound).toEqual(true);

    // condition duplicated beacause of dummy TS evaluator
    if (!('error' in parseResult)) {
      const { template: parsedTemplate, script: parsedScript, styles: parsedStyles } = parseResult;
      expect(parsedTemplate.trim()).toEqual('');
      expect(parsedScript.trim()).toEqual('');
      expect(parsedStyles).toEqual([]);
    }
  });

  it('parse function works with only script / template / style is defined', () => {
    expect(() => parse('<script>')).not.toThrowError();
    const parseResult = parse('');
    const noErrorFound = !('error' in parseResult);

    expect(noErrorFound).toEqual(true);

    // condition duplicated beacause of dummy TS evaluator
    if (!('error' in parseResult)) {
      const { template: parsedTemplate, script: parsedScript, styles: parsedStyles } = parseResult;
      expect(parsedTemplate.trim()).toEqual('');
      expect(parsedScript.trim()).toEqual('');
      expect(parsedStyles).toEqual([]);
    }
  });

  it('match snapshot for simple vue file', () => {
    const result = vueLoader.load(vueFile.fileName, vueFile.content);
    expect(result).toMatchSnapshot();
  });

  generateTestsForMatcher(vueLoader, 'vue');
});
