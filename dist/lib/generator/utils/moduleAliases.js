"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var babelUtils_1 = require("./babelUtils");
var loaderUtils_1 = require("./loaderUtils");
exports.DEFAULT_EXTENSIONS_ORDER = ['js', 'jsx', 'ts', 'tsx'];
function getUniqueFileNamesWithoutExts(files) {
    var fileExtRegexp = /(.*)\.([A-z]*)$/;
    var resultMap = {};
    Object.keys(files).forEach(function (fileName) {
        var match = fileName.match(fileExtRegexp);
        resultMap[match !== null && match.length >= 2 ? match[1] : fileName] = '';
    });
    return Object.keys(resultMap);
}
exports.getUniqueFileNamesWithoutExts = getUniqueFileNamesWithoutExts;
function findAliasForName(name, files) {
    var possibleDefaultImports = exports.DEFAULT_EXTENSIONS_ORDER.map(function (currExt) { return name + "." + currExt; });
    return possibleDefaultImports.find(function (possibleImport) { return possibleImport in files; });
}
exports.findAliasForName = findAliasForName;
function createAliasAMDModule(alias, moduleWithAlias) {
    var aliasedModuleName = loaderUtils_1.fileNameWithPackageRoot(moduleWithAlias);
    var code = "\n    import defExport from '" + aliasedModuleName + "';\n    export * from '" + aliasedModuleName + "';\n    export default defExport;\n  ";
    var aliasModuleDefinition = babelUtils_1.transform(code, alias).code;
    return loaderUtils_1.nameAMDModule(loaderUtils_1.fileNameWithPackageRoot(alias), aliasModuleDefinition);
}
exports.createAliasAMDModule = createAliasAMDModule;
function getModuleAliasesFromFiles(files) {
    var fileNames = getUniqueFileNamesWithoutExts(files);
    var moduleAliasesMap = {};
    fileNames.forEach(function (fileNameWithoutExt) {
        if (!(fileNameWithoutExt in moduleAliasesMap)) {
            var alias = findAliasForName(fileNameWithoutExt, files);
            if (alias) {
                moduleAliasesMap[fileNameWithoutExt] = alias;
            }
        }
    });
    return Object.entries(moduleAliasesMap).map(function (_a) {
        var alias = _a[0], moduleName = _a[1];
        return createAliasAMDModule(alias, moduleName);
    });
}
exports.getModuleAliasesFromFiles = getModuleAliasesFromFiles;
//# sourceMappingURL=moduleAliases.js.map