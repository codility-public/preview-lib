import { transform } from './babelUtils';
import { fileNameWithPackageRoot, nameAMDModule } from './loaderUtils';
import { Files } from '../../@types/files';

export const DEFAULT_EXTENSIONS_ORDER = ['js', 'jsx', 'ts', 'tsx'];

type ModuleAliasesMap = { [moduleName: string]: string };

export function getUniqueFileNamesWithoutExts(files: Files): Array<string> {
  const fileExtRegexp = /(.*)\.([A-z]*)$/;
  const resultMap: { [file: string]: string } = {};
  Object.keys(files).forEach(fileName => {
    const match = fileName.match(fileExtRegexp);
    resultMap[match !== null && match.length >= 2 ? match[1] : fileName] = '';
  });
  return Object.keys(resultMap);
}

export function findAliasForName(name: string, files: Files): string | undefined {
  const possibleDefaultImports = DEFAULT_EXTENSIONS_ORDER.map(currExt => `${name}.${currExt}`);
  return possibleDefaultImports.find(possibleImport => possibleImport in files);
}

export function createAliasAMDModule(alias: string, moduleWithAlias: string): string {
  const aliasedModuleName = fileNameWithPackageRoot(moduleWithAlias);
  const code = `
    import defExport from '${aliasedModuleName}';
    export * from '${aliasedModuleName}';
    export default defExport;
  `;
  const { code: aliasModuleDefinition } = transform(code, alias);
  return nameAMDModule(fileNameWithPackageRoot(alias), aliasModuleDefinition);
}

export function getModuleAliasesFromFiles(files: Files): string[] {
  const fileNames = getUniqueFileNamesWithoutExts(files);
  const moduleAliasesMap: ModuleAliasesMap = {};

  fileNames.forEach(fileNameWithoutExt => {
    if (!(fileNameWithoutExt in moduleAliasesMap)) {
      const alias = findAliasForName(fileNameWithoutExt, files);
      if (alias) {
        moduleAliasesMap[fileNameWithoutExt] = alias;
      }
    }
  });

  return Object.entries(moduleAliasesMap).map(([alias, moduleName]) =>
    createAliasAMDModule(alias, moduleName),
  );
}
