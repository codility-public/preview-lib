"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var PreviewGenerator_1 = require("../PreviewGenerator");
var loaders_1 = require("../loaders");
var __1 = require("..");
var filesMocks_1 = require("./filesMocks");
describe('PreviewGenerator', function () {
    it('match snapshot for common js project', function () {
        var dependenciesMap = filesMocks_1.jsSimpleProject.dependenciesMap, files = filesMocks_1.jsSimpleProject.files;
        var jsLoader = loaders_1.default.jsLoader, cssLoader = loaders_1.default.cssLoader, jsonLoader = loaders_1.default.jsonLoader;
        var generator = new PreviewGenerator_1.default({
            loaders: [jsLoader, cssLoader, jsonLoader],
            dependenciesMap: dependenciesMap,
        });
        var result = generator.generate(files);
        expect(result).toMatchSnapshot();
    });
    it('match snapshot for vue project', function () {
        var vuePreviewGenerator = new PreviewGenerator_1.default(__1.vueConfig);
        var result = vuePreviewGenerator.generate(filesMocks_1.vueFiles);
        expect(result).toMatchSnapshot();
    });
    it('match snapshot for react project', function () {
        var reactPreviewGenerator = new PreviewGenerator_1.default(__1.reactConfig);
        var result = reactPreviewGenerator.generate(filesMocks_1.reactFiles);
        expect(result).toMatchSnapshot();
    });
    it('should run load methods of loaders for specific files', function () {
        var jsLoader = loaders_1.default.jsLoader, cssLoader = loaders_1.default.cssLoader, jsonLoader = loaders_1.default.jsonLoader;
        var mockJSLoader = __assign(__assign({}, jsLoader), { load: jest.fn(function () { return ''; }) });
        var mockCSSLoader = __assign(__assign({}, cssLoader), { load: jest.fn(function () { return ''; }) });
        var mockJSONLoader = __assign(__assign({}, jsonLoader), { load: jest.fn(function () { return ''; }) });
        var mockedLoaders = [mockJSLoader, mockCSSLoader, mockJSONLoader];
        var generator = new PreviewGenerator_1.default({ loaders: mockedLoaders, dependenciesMap: {} });
        var files = {
            'index.css': '',
            'index.js': '',
            'index.json': '',
        };
        generator.generate(files);
        expect(mockJSONLoader.load).toHaveBeenCalledTimes(1);
        expect(mockCSSLoader.load).toHaveBeenCalledTimes(1);
        expect(mockJSLoader.load).toHaveBeenCalledTimes(1);
    });
    it('should run only first loader for specific file extension', function () {
        var jsLoader = loaders_1.default.jsLoader, jsxLoader = loaders_1.default.jsxLoader;
        var mockJSLoader = __assign(__assign({}, jsLoader), { load: jest.fn(function () { return ''; }) });
        var mockJSXLoader = __assign(__assign({}, jsxLoader), { load: jest.fn(function () { return ''; }) });
        var mockedLoaders = [mockJSLoader, mockJSXLoader];
        var generator = new PreviewGenerator_1.default({ loaders: mockedLoaders, dependenciesMap: {} });
        var files = {
            'index.js': '',
        };
        generator.generate(files);
        expect(mockJSLoader.load).toHaveBeenCalledTimes(1);
        expect(mockJSXLoader.load).toHaveBeenCalledTimes(0);
    });
});
//# sourceMappingURL=PreviewGenerator.test.js.map