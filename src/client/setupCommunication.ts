/* eslint-disable @typescript-eslint/no-explicit-any */
import { Files } from '../@types/files';

export default function setupCommunication(
  reload: (solution: Files) => void,
  debugLog: (message: string) => void,
): (type: string, data?: any) => void {
  function sendMessage(type: string, data?: any): void {
    debugLog(`[OUT] Message from preview: ${JSON.stringify({ type, data })}`);
    window.parent.postMessage({ type, ...data }, '*');
  }

  /**
   * Handle general errors that are not being logged using the console
   */
  window.addEventListener('error', () => {
    sendMessage('error');
  });

  /**
   * Monkey patch console methods so that they send a message to the parent
   */
  function monkeyPatchConsole(method: (...args: any) => void): void {
    const original = method;
    method = (...args: any): void => {
      sendMessage('log', { method });
      original.apply(console, args);
    };
  }
  monkeyPatchConsole(console.error);
  monkeyPatchConsole(console.warn);
  monkeyPatchConsole(console.log);

  function loadSolution(solution?: Files): void {
    if (solution) {
      try {
        reload(solution);
      } catch (err) {
        console.error(err);
      }
      sendMessage('solutionLoaded');
    }
  }

  window.addEventListener('message', message => {
    debugLog(`[IN] Message to preview: ${JSON.stringify(message)}`);
    if (message.data.files) {
      loadSolution(message.data.files);
    }

    if (message.data.solution) {
      loadSolution({ 'index.js': message.data.solution });
    }
  });

  /**
   * The iframe content is loaded at this point
   */
  sendMessage('loaded');

  return sendMessage;
}
