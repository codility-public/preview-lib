import PreviewGenerator from '../generator/PreviewGenerator';

export interface ClientOptions {
  previewGenerator: PreviewGenerator;
  debug?: boolean;
}
