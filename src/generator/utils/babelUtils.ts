import {
  transform as babelTransform,
  registerPlugin as babelRegisterPlugin,
  registerPreset as babelRegisterPreset,
  availablePlugins,
  availablePresets,
} from '@babel/standalone';

export function registerPlugin(name: string, plugin: object): void {
  if (!availablePlugins[name]) {
    babelRegisterPlugin(name, plugin);
  }
}

export function registerPreset(name: string, preset: object): void {
  if (!availablePresets[name]) {
    babelRegisterPreset(name, preset);
  }
}

export function transform(code: string, fileName: string, options = {}): { code: string } {
  const { plugins, presets } = { plugins: [], presets: [], ...options };

  const { code: transpiledAMDModule } = babelTransform(code, {
    presets: [
      [
        'env',
        {
          targets: {
            chrome: '45',
            edge: '16',
            firefox: '45',
            ie: '11',
            opera: '40',
            safari: '9',
          },
        },
      ],
      ...presets,
    ],
    plugins: [...plugins, 'transform-modules-amd'],
    filename: fileName,
  });

  return { code: transpiledAMDModule };
}
