import * as babelPluginVueJsx from 'babel-plugin-transform-vue-jsx';

import injectStyles from '../utils/injectStyles';
import { transform, registerPlugin } from '../utils/babelUtils';
import { fileNameAsInternalPackage, packModule, nameAMDModule } from '../utils/loaderUtils';
import { Loader } from '../../@types/loader';

interface ParseError {
  error: string;
}

interface ParseSuccess {
  script: string;
  template: string;
  styles: string[];
}

type IParseResult = ParseError | ParseSuccess;

// extract styles, scripts and template to different values
export function parse(code = ''): IParseResult {
  const element = document.createElement('div');
  element.innerHTML = code.trim();

  try {
    const templateTag = element.querySelector('template');
    const scriptTag = element.querySelector('script');
    const styleTags = Array.from(element.querySelectorAll('style')).map(node => node.innerHTML);

    return {
      template: templateTag ? templateTag.innerHTML : '',
      script: scriptTag ? scriptTag.innerHTML : '',
      styles: styleTags,
    };
  } catch (error) {
    return { error };
  }
}

function createWrapper(moduleName: string, template: string): string {
  return `
    import VueComponent from '${moduleName}';
    export * from '${moduleName}';

    export default {...VueComponent, template: \`${template}\` };
  `;
}

const VueLoader: Loader = {
  test: /.*\.vue$/,
  load(fileName: string, content: string): string {
    const parseResult = parse(content);

    if ('error' in parseResult) {
      throw new Error(`Loader for *.vue files can't parse file${fileName}`);
    }

    const { template, script, styles } = parseResult;
    const codeToTranspile = script + (styles ? injectStyles(styles.join('\n')) : '');

    registerPlugin('transform-vue-jsx', babelPluginVueJsx);

    const { code: moduleDefinition } = transform(codeToTranspile, fileName, {
      plugins: ['transform-vue-jsx'],
    });

    // We need to inject template into Vue component
    // To make it easier we deal with it on runtime

    // First we create 'internal' dependency which consists of script / styles code
    const internalModuleName = fileNameAsInternalPackage(fileName);
    const internalModule = nameAMDModule(internalModuleName, moduleDefinition);

    // After that we create target module, which will be public
    // This module will import internal module and will inject template on runtime
    const wrapperModuleCode = createWrapper(internalModuleName, template);
    const { code: wrapperModuleDefinition } = transform(wrapperModuleCode, fileName);
    const namedWrapperModule = packModule(fileName, wrapperModuleDefinition);

    // Finally we export 2 module definitions not one
    return `${internalModule}\n${namedWrapperModule}`;
  },
};

export default VueLoader;
