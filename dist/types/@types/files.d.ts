export interface Files {
    [fileName: string]: string;
}
