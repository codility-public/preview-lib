function skipSingleQuote(text: string): string {
  return text.replace(/'/g, '"');
}

export default function injectStyle(style: string): string {
  return `
        const linkElement = document.createElement('link');
          linkElement.setAttribute('rel', 'stylesheet');
          linkElement.setAttribute('type', 'text/css');
          linkElement.setAttribute(
            'href',
            '${`data:text/css;charset=UTF-8,${encodeURIComponent(skipSingleQuote(style))}`}',
            );
        const head = document.head || document.getElementsByTagName('head')[0];
        head.appendChild(linkElement);      
    `;
}
