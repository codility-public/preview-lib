"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fileExtensions = ['js', 'jsx', 'ts', 'tsx', 'json', 'svg', 'css'];
function generateTestsForMatcher(loader, ext) {
    var goodExtensions = !Array.isArray(ext) ? [ext] : ext;
    var notGoodExtensions = fileExtensions.filter(function (currExt) { return goodExtensions.indexOf(currExt) === -1; });
    it("test regexp should match only " + goodExtensions.join(' / ') + " files", function () {
        var regexp = loader.test;
        // good cases
        goodExtensions.forEach(function (currGoodExt) {
            expect(("lol." + currGoodExt).match(regexp)).toBeTruthy();
            expect(("path/lol." + currGoodExt).match(regexp)).toBeTruthy();
            expect(("codility-solution/path/lol." + currGoodExt).match(regexp)).toBeTruthy();
            expect(("lol." + currGoodExt.slice(currGoodExt.length - 1)).match(regexp)).not.toBeTruthy();
        });
        // bad real cases
        goodExtensions.forEach(function (currGoodExt) {
            notGoodExtensions.forEach(function (currBadExt) {
                expect(("lol." + currGoodExt + "." + currBadExt).match(regexp)).not.toBeTruthy();
                expect(("path/lol." + currGoodExt + "." + currBadExt).match(regexp)).not.toBeTruthy();
                expect(("codility-solution/path/lol." + currGoodExt + "." + currBadExt).match(regexp)).not.toBeTruthy();
            });
        });
        notGoodExtensions.forEach(function (currExt) {
            expect(("file." + currExt).match(regexp)).not.toBeTruthy();
        });
        // weird cases
        expect('.'.match(regexp)).not.toBeTruthy();
        expect(''.match(regexp)).not.toBeTruthy();
        expect('hi'.match(regexp)).not.toBeTruthy();
        expect('$'.match(regexp)).not.toBeTruthy();
    });
}
exports.generateTestsForMatcher = generateTestsForMatcher;
//# sourceMappingURL=utils.js.map