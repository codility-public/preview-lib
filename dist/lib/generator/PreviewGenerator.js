"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var createPreview_1 = require("./utils/createPreview");
var moduleAliases_1 = require("./utils/moduleAliases");
var PreviewGenerator = /** @class */ (function () {
    function PreviewGenerator(options) {
        var loaders = options.loaders, dependenciesMap = options.dependenciesMap;
        this.loaders = loaders;
        this.defaultDependenciesMap = dependenciesMap;
    }
    PreviewGenerator.prototype.findLoaderForFile = function (fileName) {
        if (this.loaders.length === 0) {
            return null;
        }
        var isLoaderMatched = function (loader) { return Boolean(fileName.match(loader.test)); };
        var loadersForFileType = this.loaders.filter(isLoaderMatched);
        return loadersForFileType[0];
    };
    PreviewGenerator.prototype.getModuleDefinitions = function (files) {
        var _this = this;
        var moduleDefinitions = [];
        Object.entries(files).forEach(function (_a) {
            var fileName = _a[0], solutionSource = _a[1];
            var loader = _this.findLoaderForFile(fileName);
            if (!loader) {
                throw new Error("There's no loader defined for " + fileName + ", please check if the file extension is correct.");
            }
            moduleDefinitions = __spreadArrays(moduleDefinitions, [loader.load(fileName, solutionSource)]);
        });
        return moduleDefinitions;
    };
    PreviewGenerator.prototype.getModuleAliases = function (files) {
        return moduleAliases_1.getModuleAliasesFromFiles(files);
    };
    PreviewGenerator.prototype.generate = function (files, dependenciesMap) {
        var moduleDefinitions = this.getModuleDefinitions(files);
        var moduleAliases = this.getModuleAliases(files);
        var finalDependenciesMap = dependenciesMap ? dependenciesMap : this.defaultDependenciesMap;
        return createPreview_1.default(moduleDefinitions.concat(moduleAliases), finalDependenciesMap);
    };
    return PreviewGenerator;
}());
exports.default = PreviewGenerator;
//# sourceMappingURL=PreviewGenerator.js.map