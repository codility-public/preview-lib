"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PreviewGenerator_1 = require("./PreviewGenerator");
exports.PreviewGenerator = PreviewGenerator_1.default;
var loaders_1 = require("./loaders");
exports.loaders = loaders_1.default;
var loaderUtils_1 = require("./utils/loaderUtils");
exports.packModule = loaderUtils_1.packModule;
var babelUtils_1 = require("./utils/babelUtils");
exports.registerPlugin = babelUtils_1.registerPlugin;
exports.registerPreset = babelUtils_1.registerPreset;
exports.transform = babelUtils_1.transform;
var reactConfig_1 = require("./reactConfig");
var vueConfig_1 = require("./vueConfig");
exports.reactConfig = {
    loaders: reactConfig_1.loaders,
    dependenciesMap: reactConfig_1.dependenciesMap,
};
exports.vueConfig = {
    loaders: vueConfig_1.loaders,
    dependenciesMap: vueConfig_1.dependenciesMap,
};
//# sourceMappingURL=index.js.map