export declare const loaders: import("../@types/loader").Loader[];
export declare const dependenciesMap: {
    react: string;
    'react-dom': string;
};
