"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var setupCommunication_1 = require("./setupCommunication");
var PreviewClient = /** @class */ (function () {
    function PreviewClient(options) {
        var _this = this;
        this.reload = function (solutionSource) {
            _this.debugLog('Transpiling code.');
            try {
                _this.updateIframe(_this.previewGenerator.generate(solutionSource));
            }
            catch (error) {
                console.error(error);
            }
            _this.debugLog('Code has been transpiled.');
        };
        this.debugLog = function (message) {
            if (_this.debug) {
                console.log(message);
            }
        };
        var previewGenerator = options.previewGenerator, debug = options.debug;
        this.sendMessage = null;
        this.debug = debug || false;
        this.previewGenerator = previewGenerator;
    }
    PreviewClient.prototype.updateIframe = function (html) {
        this.debugLog('Updating preview iframe.');
        var iframe = document.getElementById('preview');
        if (iframe) {
            iframe.setAttribute('srcdoc', html);
        }
        else {
            var newIframe = document.createElement('iframe');
            newIframe.setAttribute('id', 'preview');
            newIframe.setAttribute('srcdoc', html);
            var body = document.body || document.getElementsByTagName('body')[0];
            body.setAttribute('style', 'margin: 0;');
            body.appendChild(newIframe);
        }
        this.debugLog('Iframe has been updated.');
    };
    PreviewClient.prototype.init = function () {
        var styleRules = document.createElement('style');
        styleRules.innerHTML = "#preview { width: 100vw; height: 100vh; border: 0; }\n      body { margin: 0; }\n    ";
        var head = document.head || document.getElementsByTagName('head')[0];
        head.appendChild(styleRules);
        this.sendMessage = setupCommunication_1.default(this.reload, this.debugLog);
    };
    return PreviewClient;
}());
exports.default = PreviewClient;
//# sourceMappingURL=PreviewClient.js.map