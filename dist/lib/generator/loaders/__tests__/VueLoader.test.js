"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueLoader_1 = require("../VueLoader");
var utils_1 = require("./utils");
describe('VueLoader', function () {
    var template = '<div class="component"></div>';
    var style = "\n  .component { \n    width: 200px;\n    hieght: 400px;\n    background-color: red;\n  }\n  ";
    var script = "\n  export default {\n    name: 'component'\n  }\n  ";
    var vueFile = {
        fileName: 'component.vue',
        content: "\n      <template>\n      " + template + "\n      </template>\n\n      <script>\n      " + script + "\n      </script>\n\n      <style>\n      " + style + "\n      </style>\n    ",
    };
    it('parse function extracts template, style, script to different props', function () {
        var parseResult = VueLoader_1.parse(vueFile.content);
        var noErrorFound = !('error' in parseResult);
        expect(noErrorFound).toEqual(true);
        // condition duplicated beacause of dummy TS evaluator
        if (!('error' in parseResult)) {
            var parsedTemplate = parseResult.template, parsedScript = parseResult.script, parsedStyles = parseResult.styles;
            expect(parsedTemplate.trim()).toEqual(template.trim());
            expect(parsedScript.trim()).toEqual(parsedScript.trim());
            expect(parsedStyles.length).toEqual(1);
            expect(parsedStyles[0].trim()).toEqual(style.trim());
        }
    });
    it('parse function works when tags are undefined', function () {
        expect(function () { return VueLoader_1.parse(''); }).not.toThrowError();
        var parseResult = VueLoader_1.parse('');
        var noErrorFound = !('error' in parseResult);
        expect(noErrorFound).toEqual(true);
        // condition duplicated beacause of dummy TS evaluator
        if (!('error' in parseResult)) {
            var parsedTemplate = parseResult.template, parsedScript = parseResult.script, parsedStyles = parseResult.styles;
            expect(parsedTemplate.trim()).toEqual('');
            expect(parsedScript.trim()).toEqual('');
            expect(parsedStyles).toEqual([]);
        }
    });
    it('parse function works with only script / template / style is defined', function () {
        expect(function () { return VueLoader_1.parse('<script>'); }).not.toThrowError();
        var parseResult = VueLoader_1.parse('');
        var noErrorFound = !('error' in parseResult);
        expect(noErrorFound).toEqual(true);
        // condition duplicated beacause of dummy TS evaluator
        if (!('error' in parseResult)) {
            var parsedTemplate = parseResult.template, parsedScript = parseResult.script, parsedStyles = parseResult.styles;
            expect(parsedTemplate.trim()).toEqual('');
            expect(parsedScript.trim()).toEqual('');
            expect(parsedStyles).toEqual([]);
        }
    });
    it('match snapshot for simple vue file', function () {
        var result = VueLoader_1.default.load(vueFile.fileName, vueFile.content);
        expect(result).toMatchSnapshot();
    });
    utils_1.generateTestsForMatcher(VueLoader_1.default, 'vue');
});
//# sourceMappingURL=VueLoader.test.js.map