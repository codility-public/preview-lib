"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var injectStyles_1 = require("../utils/injectStyles");
var loaderUtils_1 = require("../utils/loaderUtils");
var CssLoader = {
    test: /.*\.css$/,
    load: function (fileName, content) {
        // create AMD module that will inject styles to document on load
        return "define(\"" + loaderUtils_1.fileNameWithPackageRoot(fileName) + "\", [], function () {\n            " + injectStyles_1.default(content) + "\n        });";
    },
};
exports.default = CssLoader;
//# sourceMappingURL=CssLoader.js.map