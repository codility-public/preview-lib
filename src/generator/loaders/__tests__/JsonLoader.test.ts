import jsonLoader from '../JsonLoader';
import { generateTestsForMatcher } from './utils';

describe('JsonLoader', () => {
  const file = {
    fileName: 'posts.json',
    content: `
      {
        "say": "hi"
      }
    `,
  };

  const complexFile = {
    fileName: 'complex.json',
    content: `
    [
      {
        "id": "0001",
        "type": "donut",
        "name": "Cake",
        "ppu": 0.55,
        "batters":
          {
            "batter":
              [
                { "id": "1001", "type": "Regular" },
                { "id": "1002", "type": "Chocolate" },
                { "id": "1003", "type": "Blueberry" },
                { "id": "1004", "type": "Devil's Food" }
              ]
          },
        "topping":
          [
            { "id": "5001", "type": "None" },
            { "id": "5002", "type": "Glazed" },
            { "id": "5005", "type": "Sugar" },
            { "id": "5007", "type": "Powdered Sugar" },
            { "id": "5006", "type": "Chocolate with Sprinkles" },
            { "id": "5003", "type": "Chocolate" },
            { "id": "5004", "type": "Maple" }
          ]
      },
      {
        "id": "0002",
        "type": "donut",
        "name": "Raised",
        "ppu": 0.55,
        "batters":
          {
            "batter":
              [
                { "id": "1001", "type": "Regular" }
              ]
          },
        "topping":
          [
            { "id": "5001", "type": "None" },
            { "id": "5002", "type": "Glazed" },
            { "id": "5005", "type": "Sugar" },
            { "id": "5003", "type": "Chocolate" },
            { "id": "5004", "type": "Maple" }
          ]
      },
      {
        "id": "0003",
        "type": "donut",
        "name": "Old Fashioned",
        "ppu": 0.55,
        "batters":
          {
            "batter":
              [
                { "id": "1001", "type": "Regular" },
                { "id": "1002", "type": "Chocolate" }
              ]
          },
        "topping":
          [
            { "id": "5001", "type": "None" },
            { "id": "5002", "type": "Glazed" },
            { "id": "5003", "type": "Chocolate" },
            { "id": "5004", "type": "Maple" }
          ]
      }
    ]
    `,
  };

  const brokenFile = {
    fileName: 'posts.json',
    content: `
      {
        "say": "hi"
      
      `,
  };

  it('match snapshot for json file', () => {
    const result = jsonLoader.load(file.fileName, file.content);
    expect(result).toMatchSnapshot();
  });

  it('match snapshot for complex json file', () => {
    const result = jsonLoader.load(complexFile.fileName, complexFile.content);
    expect(result).toMatchSnapshot();
  });

  it('will fail with error when the JSON is broken', () => {
    expect(() => jsonLoader.load(brokenFile.fileName, brokenFile.content)).toThrowError();
  });

  generateTestsForMatcher(jsonLoader, 'json');
});
