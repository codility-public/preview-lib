import cssLoader from '../CssLoader';
import { generateTestsForMatcher } from './utils';

describe('CssLoader', () => {
  const cssFile = {
    fileName: 'index.css',
    content: `
      .counter-button {
        padding: .3rem .8rem;
        border: 1px solid grey;
      }

      h2 {
        font-size: 100px;
      }
    `,
  };

  it('match snapshot for simple css file', () => {
    const result = cssLoader.load(cssFile.fileName, cssFile.content);
    expect(result).toMatchSnapshot();
  });

  it('does not containt single quote for css file with it', () => {
    const cssSingleQuoteFile = {
      fileName: 'test.css',
      content: `
        body {
          font-family: 'Helvetica Nue';
        }
      `,
    };
    const result = cssLoader.load(cssSingleQuoteFile.fileName, cssSingleQuoteFile.content);

    expect(result).toMatchSnapshot();
  });

  generateTestsForMatcher(cssLoader, 'css');
});
