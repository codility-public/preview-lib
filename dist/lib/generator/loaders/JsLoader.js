"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var babelUtils_1 = require("../utils/babelUtils");
var loaderUtils_1 = require("../utils/loaderUtils");
var JsLoader = {
    test: /.*\.js$/,
    load: function (fileName, content) {
        var moduleDefinition = babelUtils_1.transform(content, fileName).code;
        return loaderUtils_1.packModule(fileName, moduleDefinition);
    },
};
exports.default = JsLoader;
//# sourceMappingURL=JsLoader.js.map