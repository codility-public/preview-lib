/* eslint-disable @typescript-eslint/no-explicit-any */

// / <reference types="node" />

interface BabelOptions {
  plugins: any;
  presets: any;
  filename: string;
}

declare module '@babel/standalone' {
  export function transform(code: string, options: BabelOptions): { code: string };
  export function registerPlugin(name: string, plugin: any): void;
  export function registerPreset(name: string, preset: any): void;
  export const availablePlugins: { [key: string]: any };
  export const availablePresets: { [key: string]: any };
}

declare module '@babel/preset-env' {
  const preset: any;
  export = preset;
}
