import { PreviewClient } from './client';
import {
  loaders,
  PreviewGenerator,
  registerPlugin,
  registerPreset,
  transform,
  packModule,
} from './generator';
import { PreviewOptions } from './@types/previewOptions';

export { PreviewGenerator, vueConfig, reactConfig } from './generator';

export const loaderUtils = {
  registerPlugin,
  registerPreset,
  transform,
  loaders,
  packModule,
};

export function configurePreview(options: PreviewOptions): void {
  const { debug, loaders, dependenciesMap } = options;
  const previewGenerator = new PreviewGenerator({ loaders, dependenciesMap });
  const client = new PreviewClient({ debug, previewGenerator });
  client.init();
}
