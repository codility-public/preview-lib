export const PACKAGE_NAME = 'codility-solution';
export const INTERNAL_PACKAGE = '@@@internals';

export function fileNameWithPackageRoot(fileName: string): string {
  return `${PACKAGE_NAME}/${fileName}`;
}

export function fileNameAsInternalPackage(fileName: string): string {
  return `${INTERNAL_PACKAGE}/${fileName}`;
}

export function nameAMDModule(packageName: string, module: string): string {
  const startString = 'define([';

  if (module.indexOf(startString) === 1) {
    throw new Error('Transpiled module is not AMD. Something is wrong with babel plugins.');
  }

  return module.replace('define([', `define("${packageName}",[`);
}

export function packModule(packageName: string, module: string): string {
  return nameAMDModule(fileNameWithPackageRoot(packageName), module);
}
