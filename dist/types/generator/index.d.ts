export { default as PreviewGenerator } from './PreviewGenerator';
export { default as loaders } from './loaders';
export { packModule } from './utils/loaderUtils';
export { registerPlugin, registerPreset, transform } from './utils/babelUtils';
export declare const reactConfig: {
    loaders: import("../@types/loader").Loader[];
    dependenciesMap: {
        react: string;
        'react-dom': string;
    };
};
export declare const vueConfig: {
    loaders: import("../@types/loader").Loader[];
    dependenciesMap: {
        vue: string;
    };
};
