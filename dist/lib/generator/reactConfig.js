"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var loaders_1 = require("./loaders");
var jsxLoader = loaders_1.default.jsxLoader, cssLoader = loaders_1.default.cssLoader, jsonLoader = loaders_1.default.jsonLoader;
exports.loaders = [jsxLoader, cssLoader, jsonLoader];
// This is for development only, if you want to publish preview please upload required files with it
exports.dependenciesMap = {
    react: 'https://unpkg.com/react@16.10.1/umd/react.development',
    'react-dom': 'https://unpkg.com/react-dom@16.10.1/umd/react-dom.development',
};
//# sourceMappingURL=reactConfig.js.map