import jsxLoader from '../JsxReactLoader';
import { generateTestsForMatcher } from './utils';

describe('JsxReactLoader', () => {
  const file = {
    fileName: 'index.js',
    content: `
    import React from 'react';
    import sort from 'sorting-library';
    import Component from './component.js';

    function sortFiles(files) {
      return [...files].sort();
    }

    export class Files extends React.Component {
      // test class properties
      doSomeStuff = () => { this.setState({stuff: true}) }; 
      
      render() {
        // test spread;
        const x = ['a', 'b', 'c'];
        const y = [...x, 'd'];
        const { files } = this.props;
        return (<div>
                yesh
                </div>);
      }
    }

    export const SOME_CONSTANT = 123;

    export default sortFiles;
    `,
  };

  it('match snapshot for js file', () => {
    const result = jsxLoader.load(file.fileName, file.content);
    expect(result).toMatchSnapshot();
  });

  generateTestsForMatcher(jsxLoader, ['jsx', 'js']);
});
