import * as vueBabelPresetJsx from '@vue/babel-preset-jsx';
import { registerPreset, transform } from '../utils/babelUtils';
import { packModule } from '../utils/loaderUtils';
import { Loader } from '../../@types/loader';

const JsxVueLoader: Loader = {
  // it works with not only jsx but also js
  test: /.*\.jsx?$/,
  load(fileName: string, content: string): string {
    registerPreset('vue-babel-preset-jsx', vueBabelPresetJsx);

    const presets = ['vue-babel-preset-jsx'];
    const plugins = ['proposal-class-properties', 'syntax-object-rest-spread'];

    const { code: moduleDefinition } = transform(content, fileName, {
      presets,
      plugins,
    });

    return packModule(fileName, moduleDefinition);
  },
};

export default JsxVueLoader;
