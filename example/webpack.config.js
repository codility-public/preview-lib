module.exports = () => ({
  entry: './index.js',
  output: {
    filename: 'bundle.js',
    path: __dirname,
  },

  devtool: 'source-map',

  resolve: {
    extensions: ['.js'],
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      { enforce: 'pre', test: /\.js$/, loader: 'source-map-loader' },
    ],
  },
});
